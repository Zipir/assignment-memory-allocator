#define _DEFAULT_SOURCE
#include <stdio.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

struct block_header *heap;

void print_heap()
{
    debug_heap(stdout, heap);
    fflush(stdout);
}

static const char *const test_message[] = {
    "[\033[0;32m OK \033[0m]",
    "[\033[0;31m ERROR \033[0m]"
};


static int test1_successful_allocation() 
{
    printf("Test 1: successful allocation\n");
    fflush(stdout);

    int rc = 0;

    block_capacity capacity;
    capacity.bytes = 10 * sizeof(int);

    int *digits = _malloc(capacity.bytes);
    struct block_header *block = block_get_header(digits);

    printf("Memory allocated:\n");

    if (block == NULL) { rc = 1; }
    if (block->is_free) { rc = 1; }
    if (block->capacity.bytes < size_max(BLOCK_MIN_CAPACITY, capacity.bytes)) { rc = 1; }
    if (block->next == NULL || !block->next->is_free || block->next->next) { rc = 1; }

    for (size_t i = 0; i < 10; ++i)
    {
        block->contents[i] = i;
        printf("%i ", block->contents[i]);
    }
    printf("\n");
    print_heap();

    for (size_t i = 0; i < 10; ++i)
    {
        block->contents[i] = 0;
    }
    _free(digits);

    printf("Memory deallocated:\n");
    print_heap();

    return rc;
}

static int test2_free_one() 
{
    printf("Test 2: free one\n");
    fflush(stdout);

    int rc = 0;
    
    int *a = _malloc(sizeof(int));
    int *b = _malloc(sizeof(int));
    int *c = _malloc(sizeof(int));

    printf("Memory allocated:\n");
    print_heap();

    _free(b);

    printf("1 block deallocated:\n");
    print_heap();

    struct block_header *b1 = block_get_header(a);
    struct block_header *b2 = block_get_header(b);
    struct block_header *b3 = block_get_header(c);

    if (b1 == NULL || b2 == NULL || b3 == NULL) { rc = 1; }
    if (b1->next != b2 || b2->next != b3) { rc = 1; }
    if (b1->is_free || !b2->is_free || b3->is_free) { rc = 1; }
    if (b3->next == NULL || !b3->next->is_free) { rc = 1; }


    _free(c);
    _free(a);

    printf("Memory deallocated:\n");
    print_heap();

    return rc;
}

static int test3_free_two() 
{
    printf("Test 3: free two\n");
    fflush(stdout);

    int rc = 0;
    
    int *a = _malloc(sizeof(int));
    int *b = _malloc(sizeof(int));
    int *c = _malloc(sizeof(int));

    printf("Memory allocated:\n");
    print_heap();

    _free(a);
    _free(c);

    printf("2 blocks deallocated:\n");
    print_heap();

    struct block_header *b1 = block_get_header(a);
    struct block_header *b2 = block_get_header(b);
    struct block_header *b3 = block_get_header(c);

    if (b1 == NULL || b2 == NULL || b3 == NULL) { rc = 1; }
    if (b1->next != b2 || b2->next != b3) { rc = 1; }
    if (!b1->is_free || b2->is_free || !b3->is_free) { rc = 1; }
    if (b3->next != NULL) { rc = 1; }


    _free(b);

    printf("Memory deallocated:\n");
    print_heap();

    return rc;
}

//Тест в котором куча выделяется вплотную и проверяется
static int test4_grow_extend() 
{
    printf("Test 4: grow extend\n");
    fflush(stdout);

    int rc = 0;
    print_heap();

    int *a = _malloc(REGION_MIN_SIZE - 128);
    struct block_header *b1 = block_get_header(a);

    char *b = _malloc(b1->next->capacity.bytes + 1);
    struct block_header *b2 = block_get_header(b);

    printf("Memory allocated:\n");
    print_heap();

    if (b2 == NULL) { rc = 1; }
    if (b2->is_free) { rc = 1; }
    if (b2->next == NULL) { rc = 1; }
    if (!b2->next->is_free) { rc = 1; }
    if (b2->next->next) { rc = 1; }

    _free(b);
    _free(a);

    printf("Memory deallocated:\n");
    print_heap();

    if (b1->next) { rc = 1; }

    return rc;
}

//Тест в котором создается вспомогательная куча, не дающая расширения вплотную
static int test5_grow_no_extend() 
{
    printf("Test 5: grow no extend\n");
    fflush(stdout);

    int rc = 0;

    //Вспомогательная куча
    printf("Before:\n");
    print_heap();

    void *addr = block_after(heap);
    addr = map_pages(addr, 100 * REGION_MIN_SIZE, MAP_FIXED);

    char *a = _malloc(heap->capacity.bytes + 1);
    struct block_header *block = block_get_header(a);

    printf("Memory allocated:\n");
    print_heap();

    if (blocks_continuous(heap, block)) { rc = 1; }

    _free(a);

    printf("Memory deallocated:\n");
    print_heap();

    if (heap->next == NULL) { rc = 1; }

    return rc;
}

int main()
{
    heap = heap_init(REGION_MIN_SIZE);
    print_heap();
    printf("\n");

    int rc = test1_successful_allocation();
    printf("Test 1 %s\n\n", test_message[rc]);
    
    heap_free(heap);

    rc = test2_free_one();
    printf("Test 2 %s\n\n", test_message[rc]);

    heap_free(heap);

    rc = test3_free_two();
    printf("Test 3 %s\n\n", test_message[rc]);
    
    heap_free(heap);

    rc = test4_grow_extend();
    printf("Test 4 %s\n\n", test_message[rc]);

    heap_free(heap);

    rc = test5_grow_no_extend();
    printf("Test 5 %s\n\n", test_message[rc]);

    heap_free(heap);

    return rc;
}

